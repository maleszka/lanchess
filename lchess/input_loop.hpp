// ========================================================================== //

// Copyright (C) 2024 Adam Maleszka

// This file is part of lanchess. lanchess is free software: you can
// redistribute it and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.

// lanchess is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// lanchess. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#ifndef LCHESS_INPUT_LOOP_HPP_
#define LCHESS_INPUT_LOOP_HPP_

#include <lchess/term/key.hpp>
#include <lchess/utils/event_queue.hpp>
#include <lchess/utils/logger.hpp>

namespace lchess
{

class InputLoop
{
   protected:
    bool if_running_ = false;
    term::KeyInput ki_;
    std::shared_ptr<EventQueue> queue_;
    std::unique_ptr<std::thread> thread_;
    log::logger_t logger_;

   public:
    InputLoop(std::shared_ptr<EventQueue> queue);
    ~InputLoop();
    void exit();

   protected:
    void run();
    void sigResizeHandler(int signal);
    void sigExitHandler(int signal);
    inline static std::vector<InputLoop*> instances_;
    static void sigStaticResizeHandler(int signal)
    {
        for (InputLoop* inst : instances_) inst->sigResizeHandler(signal);
    };
    static void sigStaticExitHandler(int signal)
    {
        for (InputLoop* inst : instances_) inst->sigExitHandler(signal);
    };
};

}  // namespace lchess

#endif  // LCHESS_INPUT_LOOP_HPP_
