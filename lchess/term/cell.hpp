// ========================================================================== //

// Copyright (C) 2024 Adam Maleszka

// This file is part of lanchess. lanchess is free software: you can
// redistribute it and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.

// lanchess is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// lanchess. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#ifndef LCHESS_TERM_CELL_HPP_
#define LCHESS_TERM_CELL_HPP_

#include <lchess/term/colors.hpp>

namespace lchess::term
{

class Cell
{
   protected:
    char16_t ch_;
    TextProps props_;

   public:
    Cell(const char16_t& ch = u' ', const TextProps& props = TextProps());
    std::string encode(TermInfo* term) const;
    std::string encode(TermInfo* term, const Cell& last) const;
    bool operator==(const Cell& other) const;
    bool operator!=(const Cell& other) const;
    const char16_t& getCh() const;
    const TextProps& getProps() const;
};

}  // namespace lchess::term

#endif  // LCHESS_TERM_CELL_HPP_
