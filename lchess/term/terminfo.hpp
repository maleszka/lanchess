// ========================================================================== //

// Copyright (C) 2024 Adam Maleszka

// This file is part of lanchess. lanchess is free software: you can
// redistribute it and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.

// lanchess is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// lanchess. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#ifndef LCHESS_TERM_TERMINFO_HPP_
#define LCHESS_TERM_TERMINFO_HPP_

#include <any>
#include <lchess/term/point.hpp>
#include <memory>
#include <string>
#include <vector>

typedef struct unibi_term unibi_term;

namespace lchess::term
{

class TermInfo
{
   protected:
    const char* term_name_ = nullptr;
    std::unique_ptr<unibi_term, void (*)(unibi_term*)> term_;

   public:
    TermInfo();
    ~TermInfo();

    const char* getName() const;
    bool isOpen() const;

    std::string queryEscapeCode(std::any command,
                                std::vector<std::any> params = {});
    int queryCapability(std::any cap);
    point_t getDimensions() const;
};

}  // namespace lchess::term

#endif  // LCHESS_TERM_TERMINFO_HPP_
