// ========================================================================== //

// Copyright (C) 2024 Adam Maleszka

// This file is part of lanchess. lanchess is free software: you can
// redistribute it and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.

// lanchess is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// lanchess. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#ifndef LCHESS_TERM_KEY_HPP_
#define LCHESS_TERM_KEY_HPP_

#include <memory>
#include <string>

typedef struct TermKey TermKey;
struct pollfd;

namespace lchess::term
{

typedef struct Key
{
    bool single;
    std::string nsText;
} Key;

class KeyInput
{
   protected:
    std::unique_ptr<TermKey, void (*)(TermKey*)> tk_;
    std::unique_ptr<pollfd> poll_;

    const int short_wait_ = 50;
    const int long_wait_  = 200;

    void* key_            = nullptr;
    int current_wait_     = long_wait_;

   public:
    KeyInput();
    ~KeyInput();

    int getKey(Key& key);

   protected:
    void writeKey(Key& key);
};

}  // namespace lchess::term

#endif  // LCHESS_TERM_KEY_HPP_
