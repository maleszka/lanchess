// ========================================================================== //

// Copyright (C) 2024 Adam Maleszka

// This file is part of lanchess. lanchess is free software: you can
// redistribute it and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.

// lanchess is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// lanchess. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#include <lchess/term/cell_string.hpp>

namespace lchess::term
{

CellString::CellString()
  : std::vector<Cell>()
{
}

CellString::CellString(size_type count, const Cell& value)
  : std::vector<Cell>(count, value)
{
}

CellString::CellString(const std::u16string& us_str, const TextProps& props)
  : std::vector<Cell>(us_str.size())
{
    for (size_t i = 0; i < size(); ++i) data()[i] = Cell(us_str[i], props);
}

template<class InputIt>
CellString::CellString(InputIt first, InputIt last)
  : std::vector<Cell>(first, last)
{
}

CellString
CellString::operator+(const CellString& other) const
{
    CellString results(*this);
    return results += other;
}

const CellString&
CellString::operator+=(const CellString& other)
{
    insert(this->end(), other.begin(), other.end());
    return *this;
}

CellString
CellString::substr(size_type pos, size_type count) const
{
    return CellString(begin() + pos, begin() + pos + count);
}

}  // namespace lchess::term
