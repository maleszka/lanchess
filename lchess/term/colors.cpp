// ========================================================================== //

// Copyright (C) 2024 Adam Maleszka

// This file is part of lanchess. lanchess is free software: you can
// redistribute it and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.

// lanchess is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// lanchess. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#include <lchess/term/colors.hpp>

#include <boost/algorithm/string.hpp>
#include <iostream>
#include <sstream>
#include <string>

extern "C"
{
#include <unibilium.h>
}

namespace lchess::term
{

std::string
getFgStr(TermInfo* term, int fg)
{
    if (fg != cDefault)
        return term->queryEscapeCode(unibi_set_a_foreground, {fg});
    return "";
}

std::string
getBgStr(TermInfo* term, int bg)
{
    if (bg != cDefault)
        return term->queryEscapeCode(unibi_set_a_background, {bg});
    return "";
}

std::string
getAttrsStr(TermInfo* term, uint16_t attrs)
{
    std::string attr_str = "", attr_extra_str = "";

    // Main attributes
    if (attrs != aNone) {
        std::vector<std::any> params(9);
        for (size_t i = 0; i < 9; ++i) {
            if ((attrs >> i) & 1)
                params[i] = (int)1;
            else
                params[i] = (int)0;
        }
        attr_str = term->queryEscapeCode(unibi_set_attributes, params);
    }

    // Extra attributes (e.g. italics)
    if (attrs & aItalic) {
        attr_extra_str = term->queryEscapeCode(unibi_enter_italics_mode);
    }

    return attr_str + attr_extra_str;
}

std::string
tpEscapeCode(TermInfo* term, TextProps tp)
{
    // The order matters because attributes somehow overwrite colors
    return getAttrsStr(term, tp.attrs) + getFgStr(term, tp.fg)
           + getBgStr(term, tp.bg);
}

std::string
tpResetEscapeCode(TermInfo* term)
{
    return term->queryEscapeCode(unibi_exit_attribute_mode);
}

std::string
tpDeltaCode(TermInfo* term, const TextProps& from, const TextProps& to)
{
    // Unlucky, nothing to optimise
    if (from.attrs != to.attrs || (from.fg != cDefault && to.fg == cDefault)
        || (from.bg != cDefault && to.bg == cDefault))
    {
        return tpResetEscapeCode(term) + tpEscapeCode(term, to);
    }

    // Only colors change some we can optimise
    std::string str = "";
    if (from.fg != to.fg) str += getFgStr(term, to.fg);
    if (from.bg != to.bg) str += getBgStr(term, to.bg);
    return str;
}

void
devPrintAvailableColors(TermInfo* term)
{
    int colors = term->queryCapability(unibi_max_colors);
    for (int i = 0; i < colors; ++i) {
        std::cout << i << " " << tpEscapeCode(term, TextProps{i, -1, aNone})
                  << "colored foreground" << tpResetEscapeCode(term) << " "
                  << tpEscapeCode(term, TextProps{-1, i, aNone})
                  << "colored background" << tpResetEscapeCode(term) << "\n";
    }
}

}  // namespace lchess::term
