// ========================================================================== //

// Copyright (C) 2024 Adam Maleszka

// This file is part of lanchess. lanchess is free software: you can
// redistribute it and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.

// lanchess is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// lanchess. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#ifndef LCHESS_TERM_POINT_HPP_
#define LCHESS_TERM_POINT_HPP_

#include <cstdint>

namespace lchess::term
{

typedef struct point_t
{
    int row;
    int col;
    point_t(int r = 0, int c = 0);
    point_t operator+(const point_t& other) const;
    point_t operator-(const point_t& other) const;
} point_t;

}  // namespace lchess::term

#endif  // LCHESS_TERM_POINT_HPP_
