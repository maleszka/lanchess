// ========================================================================== //

// Copyright (C) 2024 Adam Maleszka

// This file is part of lanchess. lanchess is free software: you can
// redistribute it and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.

// lanchess is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// lanchess. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#ifndef LCHESS_TERM_COLORS_HPP_
#define LCHESS_TERM_COLORS_HPP_

#include <lchess/term/terminfo.hpp>
#include <string>

namespace lchess::term
{

enum Color
{
    cDefault = -1,
    cBlack   = 0,
    cRed     = 1,
    cGreen   = 2,
    cYellow  = 3,
    cBlue    = 4,
    cPink    = 5,
    cCyan    = 6,
    cWhite   = 7,
};

enum Attributes
{
    aNone      = 0,
    aStandout  = (1 << 0),
    aUnderline = (1 << 1),
    aReverse   = (1 << 2),
    aBlink     = (1 << 3),
    aDim       = (1 << 4),
    aBold      = (1 << 5),
    aInvis     = (1 << 6),
    aProtect   = (1 << 7),
    aItalic    = (1 << 15),
};

typedef struct TextProps
{
    int fg         = cDefault;
    int bg         = cDefault;
    uint16_t attrs = aNone;
} TextProps;

std::string tpEscapeCode(TermInfo* term, TextProps tp);
std::string tpResetEscapeCode(TermInfo* term);
std::string tpDeltaCode(TermInfo* term,
                        const TextProps& from,
                        const TextProps& to);

void devPrintAvailableColors(TermInfo* term);

}  // namespace lchess::term

#endif  // LCHESS_TERM_COLORS_HPP_
