// ========================================================================== //

// Copyright (C) 2024 Adam Maleszka

// This file is part of lanchess. lanchess is free software: you can
// redistribute it and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.

// lanchess is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// lanchess. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#ifndef LCHESS_TERM_WINDOW_HPP_
#define LCHESS_TERM_WINDOW_HPP_

#include <lchess/term/buffer.hpp>
#include <vector>

namespace lchess::term
{

const int minWindowHeight = 8;
const int minWindowWidth  = 60;

void termSetup(TermInfo* term);
void termRestoreSetup(TermInfo* term);


class Window : public Buffer
{
   protected:
    std::shared_ptr<TermInfo> term_;
    point_t cursor_;
    bool cursor_visible_ = false;

   public:
    Window(std::shared_ptr<TermInfo> term);
    ~Window();

    void write(bool force = false);
    void setCursor(point_t pos);
    void hideCursor();
};

}  // namespace lchess::term

#endif  // LCHESS_TERM_WINDOW_HPP_
