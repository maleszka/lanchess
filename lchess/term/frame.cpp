// ========================================================================== //

// Copyright (C) 2024 Adam Maleszka

// This file is part of lanchess. lanchess is free software: you can
// redistribute it and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.

// lanchess is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// lanchess. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#include <lchess/term/buffer.hpp>
#include <lchess/term/frame.hpp>
#include <lchess/utils/macros.hpp>

namespace lchess::term
{

Frame::Frame(Frame* parent, point_t offset, point_t dims, bool pad)
{
    // Find the root
    ASSERT(parent != nullptr);
    root_ = (parent->root_ == nullptr ? reinterpret_cast<Buffer*>(parent)
                                      : parent->root_);

    // Calculate absolute coordinates
    base_ = parent->base_ + offset;

    // Check if base fits in the parent
    point_t br_corner = parent->base_ + parent->dims_;
    ASSERT(base_.row < br_corner.row && base_.col < br_corner.col);

    // Calculate dims
    dims_ = (pad ? parent->dims_ - dims - offset : dims);
    ASSERT(dims_.row >= 0 && dims_.col >= 0);
    ASSERT(base_.row + dims_.row <= br_corner.row
           && base_.col + dims_.col <= br_corner.col);
}

Frame::Frame()
{
    root_ = nullptr;
    base_ = {0, 0};
}

Frame::~Frame() {}

void
Frame::putCell(point_t pos, const Cell& c)
{
    if (pos.row >= dims_.row || pos.col >= dims_.col) return;
    if (root_ == nullptr) return;
    root_->putCell(base_ + pos, c);
}

void
Frame::putCh(point_t pos, const char16_t& ch, const TextProps& props)
{
    putCell(pos, Cell(ch, props));
}

void
Frame::putStr(point_t pos, const std::u16string& str, const TextProps& props)
{
    for (size_t i = 0; i < str.size(); ++i)
        putCell(point_t(pos.row, pos.col + i), Cell(str[i], props));
}

void
Frame::putStr(point_t pos, const CellString& str)
{
    for (size_t i = 0; i < str.size(); ++i)
        putCell(point_t(pos.row, pos.col + i), str[i]);
}

void
Frame::setCursor(point_t pos)
{
    ASSERT(pos.row >= 0 && pos.col >= 0);
    ASSERT(pos.row < dims_.row && pos.col < dims_.col);
    root_->setCursor(pos + base_);
}

void
Frame::hideCursor()
{
    root_->hideCursor();
}

const point_t&
Frame::getDimensions() const
{
    return dims_;
}

}  // namespace lchess::term
