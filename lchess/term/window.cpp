// ========================================================================== //

// Copyright (C) 2024 Adam Maleszka

// This file is part of lanchess. lanchess is free software: you can
// redistribute it and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.

// lanchess is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// lanchess. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //


#include <lchess/term/window.hpp>

#include <iostream>

extern "C"
{
#include <unibilium.h>
}

namespace lchess::term
{

static bool ifTermHasBeenSetup = false;

void
termSetup(TermInfo* term)
{
    if (ifTermHasBeenSetup) return;
    ifTermHasBeenSetup = true;

    std::cout.sync_with_stdio(0);
    std::cin.sync_with_stdio(0);
    std::cout.tie(0);
    std::cin.tie(0);

    std::cout << term->queryEscapeCode(unibi_enter_ca_mode);
    std::cout << term->queryEscapeCode(unibi_cursor_invisible);
    std::cout.flush();
}

void
termRestoreSetup(TermInfo* term)
{
    if (!ifTermHasBeenSetup) return;
    ifTermHasBeenSetup = false;

    std::cout << term->queryEscapeCode(unibi_exit_ca_mode);
    std::cout << term->queryEscapeCode(unibi_cursor_normal);
    std::cout.flush();

    std::cout.sync_with_stdio(1);
    std::cin.sync_with_stdio(1);
}

Window::Window(std::shared_ptr<TermInfo> term)
  : Buffer(term->getDimensions())
  , term_(term)
{
    std::cout << term_->queryEscapeCode(unibi_clear_screen);
    std::cout.flush();
}

Window::~Window()
{
    hideCursor();
}

void
Window::write(bool force)
{
    // Make sure that cursor is currently invisible
    if (cursor_visible_)
        std::cout << term_->queryEscapeCode(unibi_cursor_invisible);

    std::cout << term_->queryEscapeCode(unibi_cursor_home);
    for (int line = 0; line < dims_.row; ++line) {
        // Buffer has changed
        if (force || touched_[line]) {
            for (int col = 0; col < dims_.col; ++col) {
                std::cout << data_[line][col].encode(
                  term_.get(), (col > 0 ? data_[line][col - 1] : Cell()));
            }
            std::cout << tpResetEscapeCode(term_.get());
            touched_[line] = false;
        }

        // End all lines, excluding the last one
        if (line + 1 < dims_.row) std::cout << "\n";
    }

    // Make cursor visible back, if was focuses in some input box
    if (cursor_visible_) setCursor(cursor_);

    std::cout.flush();
}

void
Window::setCursor(point_t pos)
{
    cursor_ = pos;
    std::cout << term_->queryEscapeCode(unibi_cursor_address,
                                        {cursor_.row, cursor_.col});
    std::cout << term_->queryEscapeCode(unibi_cursor_normal);
    std::cout.flush();
    cursor_visible_ = true;
}

void
Window::hideCursor()
{
    cursor_visible_ = false;
    std::cout << term_->queryEscapeCode(unibi_cursor_invisible);
    std::cout.flush();
}

}  // namespace lchess::term
