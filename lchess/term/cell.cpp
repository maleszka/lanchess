// ========================================================================== //

// Copyright (C) 2024 Adam Maleszka

// This file is part of lanchess. lanchess is free software: you can
// redistribute it and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.

// lanchess is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// lanchess. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#include <lchess/term/cell.hpp>
#include <utf8.h>

namespace lchess::term
{

Cell::Cell(const char16_t& ch, const TextProps& props)
  : ch_(ch)
  , props_(props)
{
}

std::string
Cell::encode(TermInfo* term) const
{
    return tpEscapeCode(term, props_) + utf8::utf16to8(std::u16string{ch_})
           + tpResetEscapeCode(term);
}

std::string
Cell::encode(TermInfo* term, const Cell& last) const
{
    return tpDeltaCode(term, last.props_, props_)
           + utf8::utf16to8(std::u16string{ch_});
}

bool
Cell::operator==(const Cell& other) const
{
    return ch_ == other.ch_ && props_.fg == other.props_.fg
           && props_.bg == other.props_.bg
           && props_.attrs == other.props_.attrs;
}

bool
Cell::operator!=(const Cell& other) const
{
    return !operator==(other);
}

const char16_t&
Cell::getCh() const
{
    return ch_;
}

const TextProps&
Cell::getProps() const
{
    return props_;
}

}  // namespace lchess::term
