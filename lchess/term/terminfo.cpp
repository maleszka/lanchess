// ========================================================================== //

// Copyright (C) 2024 Adam Maleszka

// This file is part of lanchess. lanchess is free software: you can
// redistribute it and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.

// lanchess is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// lanchess. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //


#include <lchess/term/terminfo.hpp>

#include <cstdlib>
#include <sys/ioctl.h>
#include <unistd.h>

extern "C"
{
#include <unibilium.h>
}

namespace lchess::term
{

TermInfo::TermInfo()
  : term_name_(std::getenv("TERM"))
  , term_((term_name_ == nullptr ? nullptr : unibi_from_term(term_name_)),
          unibi_destroy)
{
}

TermInfo::~TermInfo() {}

const char*
TermInfo::getName() const
{
    return term_name_;
}

bool
TermInfo::isOpen() const
{
    return term_ != nullptr;
}

// TODO: Measure performance, it's an critical component
std::string
TermInfo::queryEscapeCode(std::any command_any,
                          std::vector<std::any> params_any)
{
    // Cast std::any to unibi types
    unibi_string command = std::any_cast<unibi_string>(command_any);
    unibi_var_t params[9];
    for (size_t i = 0; i < 9 && i < params_any.size(); ++i) {
        auto& pa = params_any[i];
        auto& p  = params[i];
        if (pa.type() == typeid(int))
            p = unibi_var_from_num(std::any_cast<int>(pa));
        else if (pa.type() == typeid(char*))
            p = unibi_var_from_str(std::any_cast<char*>(pa));
    }

    // Obtain format data
    const char* format = unibi_get_str(term_.get(), command);
    if (format == nullptr) return "";

    // Format the string with parameters
    std::string results;
    unibi_var_t format_vars[26 + 26] = {{0, nullptr}};
    auto format_out = [](void* results, const char* data, size_t data_size)
    { reinterpret_cast<std::string*>(results)->append(data, data_size); };
    unibi_format(format_vars,
                 format_vars + 26,
                 format,
                 params,
                 format_out,
                 &results,
                 nullptr,
                 nullptr);

    return results;
}

int
TermInfo::queryCapability(std::any cap)
{
    if (cap.type() == typeid(unibi_numeric))
        return unibi_get_num(term_.get(), std::any_cast<unibi_numeric>(cap));
    else
        return unibi_get_bool(term_.get(), std::any_cast<unibi_boolean>(cap));
}

point_t
TermInfo::getDimensions() const
{
    struct winsize w;
    ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
    return point_t(w.ws_row, w.ws_col);
}

}  // namespace lchess::term
