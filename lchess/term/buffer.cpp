// ========================================================================== //

// Copyright (C) 2024 Adam Maleszka

// This file is part of lanchess. lanchess is free software: you can
// redistribute it and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.

// lanchess is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// lanchess. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#include <algorithm>
#include <lchess/term/buffer.hpp>
#include <lchess/utils/macros.hpp>

namespace lchess::term
{

Buffer::Buffer(Frame* parent)
  : Buffer(parent->dims_)
{
    setOutput(parent);
}

Buffer::Buffer(point_t dims)
{
    dims_ = dims;

    // Allocate the buffer's contents
    touched_.resize(dims_.row, true);
    data_.resize(dims_.row, std::vector<Cell>(dims_.col));
}

void
Buffer::setOutput(Frame* parent)
{
    ASSERT(parent->getDimensions().row == dims_.row
                && parent->getDimensions().col == dims_.col);
    output_      = (parent->root_ == nullptr ? reinterpret_cast<Buffer*>(parent)
                                             : parent->root_);
    output_base_ = parent->base_;
}

void
Buffer::putCell(point_t pos, const Cell& c)
{
    // Make sure that the position is valid
    ASSERT(pos.row >= 0 && pos.col >= 0);
    ASSERT(pos.row < dims_.row && pos.col < dims_.col);

    // Save the data
    if (data_[pos.row][pos.col] != c) {
        data_[pos.row][pos.col] = c;
        touched_[pos.row]       = true;
    }
}

const Cell&
Buffer::getCell(point_t pos)
{
    return data_[pos.row][pos.col];
}

void
Buffer::setCursor(point_t pos)
{
    ASSERT(pos.row >= 0 && pos.col >= 0);
    ASSERT(pos.row < dims_.row && pos.col < dims_.col);
    output_->setCursor(pos + output_base_);
}

void
Buffer::hideCursor()
{
    output_->hideCursor();
}

void
Buffer::write(bool force)
{
    ASSERT(output_ != nullptr);
    for (size_t line = 0; line < touched_.size(); ++line) {
        if (force || touched_[line]) {
            output_->copyLine(output_base_ + point_t(line, 0),
                              data_[line].begin(),
                              data_[line].end());
            touched_[line] = false;
        }
    }
}

void
Buffer::clear()
{
    touched_ = std::vector<bool>(dims_.row, true);
    data_
      = std::vector<std::vector<Cell>>(dims_.row, std::vector<Cell>(dims_.col));
}

void
Buffer::copyLine(point_t start,
                 std::vector<Cell>::const_iterator first,
                 std::vector<Cell>::const_iterator last)
{
    if (!std::equal(first, last, data_[start.row].begin() + start.col)) {
        touched_[start.row] = true;
        std::copy(first, last, data_[start.row].begin() + start.col);
    }
}

std::vector<Cell>::const_iterator
Buffer::rowBegin(int row) const
{
    return data_[row].begin();
}

std::vector<Cell>::const_iterator
Buffer::rowEnd(int row) const
{
    return data_[row].end();
}

}  // namespace lchess::term
