// ========================================================================== //

// Copyright (C) 2024 Adam Maleszka

// This file is part of lanchess. lanchess is free software: you can
// redistribute it and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.

// lanchess is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// lanchess. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#include <lchess/term/key.hpp>
#include <poll.h>
#include <stdexcept>
#include <termkey.h>

namespace lchess::term
{

KeyInput::KeyInput()
  : tk_(termkey_new(0, TERMKEY_FLAG_CTRLC | TERMKEY_FLAG_UTF8), termkey_destroy)
  , poll_(new pollfd)
{
    if (tk_ == nullptr)
        std::runtime_error(
          "cannot initialize termkey handle, make sure terminfo is loadable");

    poll_->fd     = 0;
    poll_->events = POLLIN;

    key_          = new TermKeyKey;
}

KeyInput::~KeyInput()
{
    if (key_ != nullptr) delete (TermKeyKey*)key_;
}

int
KeyInput::getKey(Key& key)
{
    TermKeyResult result;

    // Wait on fd for changes
    int poll_rt = poll(poll_.get(), 1, current_wait_);
    if (poll_rt < 0) {
        // Poll internal failure
        return -1;
    } else if (poll_rt == 0 && current_wait_ == short_wait_) {
        // Multibyte input aborted because of a timeout
        result        = termkey_getkey_force(tk_.get(), (TermKeyKey*)key_);
        current_wait_ = long_wait_;
        if (result == TERMKEY_RES_KEY) {
            writeKey(key);
            return 1;
        }
        return 0;
    } else {
        // Load changes
        if (poll_->revents & (POLLIN | POLLHUP | POLLERR))
            termkey_advisereadable(tk_.get());

        // Read key
        result = termkey_getkey(tk_.get(), (TermKeyKey*)key_);
        if (result == TERMKEY_RES_KEY) {
            writeKey(key);
            current_wait_ = 0;
            return 1;
        } else if (result == TERMKEY_RES_NONE) {
            current_wait_ = long_wait_;
            return 0;
        } else if (result == TERMKEY_RES_AGAIN) {
            current_wait_ = short_wait_;
            return 0;
        } else {
            current_wait_ = long_wait_;
            return -1;
        }
    }
}

void
KeyInput::writeKey(Key& out)
{
    TermKeyKey* key = (TermKeyKey*)key_;

    // Return unicode key as it is
    if (key->type == TERMKEY_TYPE_UNICODE && key->modifiers == 0) {
        out.single = true;
        out.nsText = key->utf8;
        return;
    }

    // Return formatted keybind
    char* buffer = new char[32];
    termkey_strfkey(tk_.get(), buffer, 32, key, TERMKEY_FORMAT_VIM);
    out.single = false;
    out.nsText = (buffer == nullptr ? "" : buffer);

    delete[] buffer;
    return;
}

}  // namespace lchess::term
