// ========================================================================== //

// Copyright (C) 2024 Adam Maleszka

// This file is part of lanchess. lanchess is free software: you can
// redistribute it and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.

// lanchess is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// lanchess. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#ifndef LCHESS_TERM_CELL_STRING_HPP_
#define LCHESS_TERM_CELL_STRING_HPP_

#include <lchess/term/cell.hpp>

namespace lchess::term
{

class CellString : public std::vector<Cell>
{
   public:
    CellString();

    CellString(size_type count, const Cell& value = Cell());

    CellString(const std::u16string& us_str,
               const TextProps& props = TextProps());

    template<class InputIt>
    CellString(InputIt first, InputIt last);

    CellString operator+(const CellString& other) const;
    const CellString& operator+=(const CellString& other);

    CellString substr(size_type pos = 0, size_type count = -1) const;
};

}  // namespace lchess::term

#endif  // LCHESS_TERM_CELL_STRING_HPP_
