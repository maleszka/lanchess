// ========================================================================== //

// Copyright (C) 2024 Adam Maleszka

// This file is part of lanchess. lanchess is free software: you can
// redistribute it and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.

// lanchess is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// lanchess. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#ifndef LCHESS_LCHESS_HPP_
#define LCHESS_LCHESS_HPP_

#include <lchess/config/config.hpp>
#include <lchess/input_loop.hpp>
#include <lchess/utils/logger.hpp>

namespace lchess
{

class LChess
{
   protected:
    bool ready_      = false;
    bool is_running_ = false;
    log::logger_t logger_;
    std::unique_ptr<config::Config> config_;
    std::unique_ptr<InputLoop> input_loop_;
    std::shared_ptr<EventQueue> events_;

   public:
    LChess(int argc, char* argv[]) noexcept;
    ~LChess();
    void run();

   protected:
    bool handleEvent(const Event& event);
};

}  // namespace lchess

#endif  // LCHESS_LCHESS_HPP_
