// ========================================================================== //

// Copyright (C) 2024 Adam Maleszka

// This file is part of lanchess. lanchess is free software: you can
// redistribute it and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.

// lanchess is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// lanchess. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#include <lchess/utils/event_queue.hpp>

namespace lchess
{

void
EventQueue::pushEvent(const Event& event)
{
    std::unique_lock lock(mutex_);
    events_.push(event);
    lock.unlock();
    cv_.notify_one();  // notify that the queue is not empty
}

void
EventQueue::emplaceEvent(const EventType& type,
                         const std::any& data,
                         EventPriority priority)
{
    std::unique_lock lock(mutex_);
    events_.emplace(Event{type, data, priority});
    lock.unlock();
    cv_.notify_one();  // notify that the queue is not empty
}

Event
EventQueue::getEvent()
{
    std::unique_lock lock(mutex_);
    cv_.wait(lock, [this]() { return !events_.empty(); });
    Event ev = events_.top();
    events_.pop();
    return ev;
}

size_t
EventQueue::size()
{
    std::unique_lock lock(mutex_);
    return events_.size();
}

bool
EventQueue::empty()
{
    std::unique_lock lock(mutex_);
    return events_.empty();
}

}  // namespace lchess
