// ========================================================================== //

// Copyright (C) 2024 Adam Maleszka

// This file is part of lanchess. lanchess is free software: you can
// redistribute it and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.

// lanchess is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// lanchess. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#ifndef LCHESS_UTILS_LOGGER_HPP_
#define LCHESS_UTILS_LOGGER_HPP_

#include <boost/log/attributes/named_scope.hpp>
#include <boost/log/sources/record_ostream.hpp>
#include <boost/log/sources/severity_logger.hpp>

namespace lchess::log
{

enum class SeverityLevel
{
    debug    = 0,
    info     = 1,
    warning  = 2,
    error    = 3,
    critical = 4,
};

typedef boost::log::sources::severity_logger<SeverityLevel> logger_t;

void flush();

}  // namespace lchess::log

#define LOG(lg, severity) BOOST_LOG_SEV(lg, lchess::log::SeverityLevel::severity)

#endif  // LCHESS_UTILS_LOGGER_HPP_
