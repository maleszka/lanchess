// ========================================================================== //

// Copyright (C) 2024 Adam Maleszka

// This file is part of lanchess. lanchess is free software: you can
// redistribute it and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.

// lanchess is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// lanchess. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#include <boost/core/null_deleter.hpp>
#include <boost/filesystem/path.hpp>
#include <boost/format.hpp>
#include <boost/log/attributes.hpp>
#include <boost/log/core.hpp>
#include <boost/log/expressions.hpp>
#include <fstream>
#include <iostream>
#include <lchess/utils/logger_sinks.hpp>

namespace lchess::log
{
// Create keywords for attributes
BOOST_LOG_ATTRIBUTE_KEYWORD(severity, "Severity", lchess::log::SeverityLevel)
BOOST_LOG_ATTRIBUTE_KEYWORD(record_id, "RecordID", unsigned int)
BOOST_LOG_ATTRIBUTE_KEYWORD(timestamp, "TimeStamp", boost::posix_time::ptime)

// Boost log namespace shortcuts
namespace expr  = boost::log::expressions;
namespace attrs = boost::log::attributes;


/*******************************************************************************
 * Boost.log configuration
 ******************************************************************************/

void
setupCore()
{
    boost::shared_ptr<boost::log::core> core = boost::log::core::get();

    // Add more logging attributes
    core->add_global_attribute("Scope", attrs::named_scope());
    core->add_global_attribute("RecordID", attrs::counter<unsigned int>());
    core->add_global_attribute("TimeStamp", attrs::local_clock());
}

void
setSinkVerbosity(boost::shared_ptr<sink_t> sink, SeverityLevel sev_thes)
{
    sink->set_filter(severity >= sev_thes);
}

boost::shared_ptr<sink_t>
setupSink(boost::shared_ptr<std::ostream> output,
          SeverityLevel sev_thes,
          std::function<void(const boost::log::record_view&,
                             boost::log::formatting_ostream& strm)> formatter)
{
    boost::shared_ptr<boost::log::core> core = boost::log::core::get();

    // Initialize sink
    boost::shared_ptr<sink_t> log_sink = boost::make_shared<sink_t>();
    log_sink->locked_backend()->add_stream(output);
    log_sink->set_formatter(formatter);
    core->add_sink(log_sink);

    // Set filtering
    setSinkVerbosity(log_sink, sev_thes);

    return log_sink;
}


/*******************************************************************************
 * Basic formatters
 ******************************************************************************/

std::string
severityString(const SeverityLevel& sev, bool color = false)
{
    switch (sev) {
        case SeverityLevel::debug:
            return "Debug    ";
        case SeverityLevel::info:
            return "Info     ";
        case SeverityLevel::warning:
            return color ? "\033[0;35mWarning\033[0m  " : "Warning  ";
        case SeverityLevel::error:
            return color ? "\033[0;31mError\033[0m    " : "Error    ";
        case SeverityLevel::critical:
            return color ? "\033[1;31mCritical\033[0m " : "Critical ";
    }
    return "";
}

std::string
namedScopeToStr(const boost::log::attributes::named_scope::value_type& scope)
{
    if (scope.empty()) return "";
    std::string file
      = boost::filesystem::path(scope.back().file_name.c_str()).stem().string();
    unsigned int line = scope.back().line;
    return (boost::format("%1%:%2%") % file % line).str();
}

void
basicClogFormatter(const boost::log::record_view& rec,
                   boost::log::formatting_ostream& strm)
{
    // Obtain scope info
    std::string nscope = namedScopeToStr(
      boost::log::extract<attrs::named_scope::value_type>("Scope", rec).get());

    // Format final message
    // clang-format off
  strm << boost::format("%1% %2$-18s  %3%")
          % severityString(rec[severity].get(), true)
          % nscope
          % rec[expr::smessage];
    // clang-format on
}

void
basicFileLogFormatter(const boost::log::record_view& rec,
                      boost::log::formatting_ostream& strm)
{
    // Obtain scope info
    std::string nscope = namedScopeToStr(
      boost::log::extract<attrs::named_scope::value_type>("Scope", rec).get());

    // Format final message
    auto time = rec[timestamp]->time_of_day();
    // clang-format off
  strm << boost::format("%1$-40s %2% %3%")
          % (boost::format("[%1%][%2%][%3%]")
             % rec[record_id]
             % (boost::format("%1$02d:%2$02d:%3$02d.%4%")
                % time.hours()
                % time.minutes()
                % time.seconds()
                % (time.fractional_seconds() / 100))
             % nscope)
          % severityString(rec[severity].get())
          % rec[expr::smessage];
    // clang-format on
}


/*******************************************************************************
 * Basic sink configuration
 ******************************************************************************/

boost::shared_ptr<sink_t>
setupBasicFileSink(std::string path, SeverityLevel sev_thes)
{
    return setupSink(
      boost::shared_ptr<std::ostream>(new std::ofstream(path.c_str())),
      sev_thes,
      &basicFileLogFormatter);
}

boost::shared_ptr<sink_t>
setupBasicClogSink(SeverityLevel sev_thes)
{
    return setupSink(
      boost::shared_ptr<std::ostream>(&std::clog, boost::null_deleter()),
      sev_thes,
      &basicClogFormatter);
}
}  // namespace lchess::log
