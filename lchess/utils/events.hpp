// ========================================================================== //

// Copyright (C) 2024 Adam Maleszka

// This file is part of lanchess. lanchess is free software: you can
// redistribute it and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.

// lanchess is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// lanchess. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#ifndef LCHESS_UTILS_EVENTS_HPP_
#define LCHESS_UTILS_EVENTS_HPP_

#include <any>
#include <iostream>

namespace lchess
{

enum class EventType
{
    // Input
    keyPressed,
    inputFailure,

    // Signals
    sigResize,
    sigExit,

    // Actions
    actSessionExit,

    // Critical
    exception,
};

enum class EventPriority
{
    input    = 0,  //!< for user input
    signal   = 1,  //!< for important input
    action   = 2,  //!< for usual actions (e.g. receive a move)
    critical = 3,  //!< for errors
};

typedef struct Event
{
    EventType type;
    std::any data;
    EventPriority priority;
} Event;

std::ostream& operator<<(std::ostream& os, const Event& event);
bool operator<(const Event& a, const Event& b);

}  // namespace lchess

#endif  // LCHESS_UTILS_EVENTS_HPP_
