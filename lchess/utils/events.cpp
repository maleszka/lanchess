// ========================================================================== //

// Copyright (C) 2024 Adam Maleszka

// This file is part of lanchess. lanchess is free software: you can
// redistribute it and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.

// lanchess is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// lanchess. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#include <lchess/utils/events.hpp>

namespace lchess
{

std::ostream&
operator<<(std::ostream& os, const Event& event)
{
    std::string event_name;
    switch (event.type) {
        case EventType::keyPressed:
            event_name = "keyPressed";
            break;
        case EventType::inputFailure:
            event_name = "inputFailure";
            break;
        case EventType::sigResize:
            event_name = "sigResize";
            break;
        case EventType::sigExit:
            event_name = "sigExit";
            break;
        case EventType::actSessionExit:
            event_name = "actSessionExit";
            break;
        case EventType::exception:
            event_name = "exception";
            break;
    }
    return os << "'" << event_name << "', priority " << (int)event.priority;
}

bool
operator<(const Event& a, const Event& b)
{
    return a.priority < b.priority;
}

}  // namespace lchess
