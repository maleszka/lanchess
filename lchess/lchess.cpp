// ========================================================================== //

// Copyright (C) 2024 Adam Maleszka

// This file is part of lanchess. lanchess is free software: you can
// redistribute it and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.

// lanchess is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// lanchess. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#include <boost/format.hpp>
#include <iostream>
#include <lchess/lchess.hpp>
#include <lchess/utils/config.hpp>
#include <lchess/utils/event_queue.hpp>
#include <lchess/utils/logger_sinks.hpp>

namespace lchess
{

LChess::LChess(int argc, char* argv[]) noexcept
{
    BOOST_LOG_FUNCTION();

    // Setup initial logging
    lchess::log::setupCore();
    auto log_sink
      = lchess::log::setupBasicClogSink(lchess::log::SeverityLevel::error);

    // Process command line arguments
    try {
        config_.reset(new config::Config(argc, argv));
    } catch (const std::exception& e) {
        LOG(logger_, critical)
          << boost::format("failed parsing cli arguments: %1%") % e.what();
        return;
    }

    // Reset sink verbosity according to new verbosity settings
    log_sink->flush();
    log::setSinkVerbosity(log_sink, config_->getSevThes());
    LOG(logger_, info) << "logging successfully configured, with threshold "
                            + std::to_string((int)config_->getSevThes());

    // Ensure that lchess is run inside a tty
    if (!isatty(fileno(stdout))) {
        LOG(logger_, critical)
          << "lchess must be run inside an interactive tty (no pipe!)";
        return;
    }

    ready_ = true;
}

LChess::~LChess() {}

void
LChess::run()
{
    if (!ready_) return;

    // Display help message
    if (config_->getIfHelp()) {
        std::cout << config_->getHelpMessage() << "\n";
        return;
    }

    // Display version string
    if (config_->getIfVersion()) {
        std::cout << "LANChess version " << LCHESS_VERSION << "\n";
        return;
    }

    // Start input loop
    events_.reset(new EventQueue());
    input_loop_.reset(new InputLoop(events_));

    // Run events dispatcher
    LOG(logger_, info) << "starting events dispatcher";
    uint32_t processed_events = 0;
    is_running_               = true;
    while (is_running_) {
        // Obtain an event
        const auto& event = events_->getEvent();
        processed_events++;
        LOG(logger_, debug)
          << "got event " << processed_events << ": " << event;

        // Process the event
        if (!handleEvent(event))
            LOG(logger_, debug) << "unhandled event " << processed_events;

        // Write log
        log::flush();
    }
    input_loop_.reset();
    LOG(logger_, info) << "LANChess session ended";
}

bool
LChess::handleEvent(const Event& event)
{
    BOOST_LOG_FUNCTION();

    switch (event.type) {
        case EventType::keyPressed: {
            const auto& key = std::any_cast<term::Key>(event.data);
            LOG(logger_, info) << "key pressed: '" << key.nsText << "'";

            // Exit session
            if (key == config_->getKeybinds().sessionExit) {
                events_->emplaceEvent(
                  EventType::actSessionExit,
                  std::string("ending session due to a key press"),
                  EventPriority::action);
                return true;
            }
            break;
        }
        case EventType::inputFailure: {
            // TODO: test when it occurs and handle it correctly
            break;
        }
        case EventType::sigResize: {
            // TODO: handle it
            break;
        }
        case EventType::sigExit: {
            events_->emplaceEvent(
              EventType::actSessionExit,
              std::string("ending session due to a kernel signal"),
              EventPriority::action);
            return true;
        }
        case EventType::actSessionExit: {
            LOG(logger_, info) << std::any_cast<std::string>(event.data);
            is_running_ = false;
            return true;
        }
        case EventType::exception: {
            // TODO: decide how to use it and handle it correctly
            break;
        }
    }
    return false;
}

}  // namespace lchess
