// ========================================================================== //

// Copyright (C) 2024 Adam Maleszka

// This file is part of lanchess. lanchess is free software: you can
// redistribute it and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.

// lanchess is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// lanchess. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#ifndef LCHESS_CONFIG_CONFIG_HPP_
#define LCHESS_CONFIG_CONFIG_HPP_

#include <lchess/config/keybinds.hpp>
#include <lchess/utils/logger.hpp>
#include <memory>
#include <string>

namespace boost::program_options
{
class options_description;
}

namespace lchess::config
{

namespace po = boost::program_options;

class Config
{
   protected:
    Keybinds keybinds_;

    std::string lchess_command_;
    std::string listen_address_;
    std::string srv_address_;  // e.g. localhost:71238
    std::string srv_password_;
    bool if_srv_starts_;
    bool if_help_;
    bool if_version_;
    int verbosity_;

    std::unique_ptr<po::options_description> od_cli_;

   public:
    Config(int argc, char* argv[]);
    ~Config();

    const std::string& getListenAddress() const;
    const std::string& getSrvAddress() const;
    const std::string& getSrvPassword() const;
    const bool& getIfSrvStarts() const;
    const bool& getIfHelp() const;
    const bool& getIfVersion() const;
    log::SeverityLevel getSevThes() const;
    const Keybinds& getKeybinds() const;

    std::string getHelpMessage() const;

   protected:
    void makeOdCli();
};

}  // namespace lchess::config

#endif  // LCHESS_CONFIG_CONFIG_HPP_
