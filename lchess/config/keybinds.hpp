// ========================================================================== //

// Copyright (C) 2024 Adam Maleszka

// This file is part of lanchess. lanchess is free software: you can
// redistribute it and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.

// lanchess is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// lanchess. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#ifndef LCHESS_CONFIG_KEYBINDS_HPP_
#define LCHESS_CONFIG_KEYBINDS_HPP_

#include <string>
#include <vector>

namespace lchess::term
{
typedef struct Key Key;
}

namespace lchess::config
{

struct Keybind
{
    std::vector<std::string> keys;
    bool operator==(const term::Key& key) const;
};
std::istream& operator>>(std::istream& is, Keybind& kbd);

struct Keybinds
{
    // Session
    Keybind sessionExit = {{"<C-c>"}};
};

}  // namespace lchess::config

#endif  // LCHESS_CONFIG_KEYBINDS_HPP_
