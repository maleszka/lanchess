// ========================================================================== //

// Copyright (C) 2024 Adam Maleszka

// This file is part of lanchess. lanchess is free software: you can
// redistribute it and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.

// lanchess is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// lanchess. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#include <boost/format.hpp>
#include <boost/program_options.hpp>
#include <lchess/config/config.hpp>
#include <lchess/utils/logger.hpp>

namespace lchess::config
{

Config::Config(int argc, char* argv[])
{
    makeOdCli();

    // Parse command arguments
    lchess_command_ = argv[0];
    po::variables_map vm;
    try {
        auto parsed_opts
          = po::command_line_parser(argc, argv).options(*od_cli_).run();
        po::store(parsed_opts, vm);
    } catch (const std::exception& e) {
        // Parse empty options to obtain default values
        char* args[] = {argv[0]};
        po::store(po::command_line_parser(1, args).options(*od_cli_).run(), vm);
        throw;
    }

    // Write vm to Config's members
    po::notify(vm);

    // Make sure that format of options is correct
    // TODO: check listen_address and server address
    {
        if (!listen_address_.empty() && !srv_address_.empty())
            throw std::runtime_error(
              "listen and server cannot be specified at the same time");

        if (!(verbosity_ >= 0 && verbosity_ <= 4))
            throw std::runtime_error("verbosity must be in range 0-4");
    }
}

void
Config::makeOdCli()
{
    od_cli_.reset(new po::options_description("Options"));
    od_cli_->add_options()  //
      ("listen,l",
       po::value<std::string>(&listen_address_)
         ->default_value("localhost:71238"),
       "address to listen on in server mode")  //

      ("server,s",
       po::value<std::string>(&srv_address_)->default_value(""),
       "server to connect to")  //

      ("pass,p",
       po::value<std::string>(&srv_password_)->default_value(""),
       "session password")  //

      ("start",
       po::bool_switch(&if_srv_starts_),
       "whether host player should start")  //

      ("verbosity,v",
       po::value<int>(&verbosity_)->default_value(0),
       "logging (0 critical, 4 debug)")  //

      ("help,h", po::bool_switch(&if_help_), "produce help message")  //

      ("version", po::bool_switch(&if_version_), "show version info")  //
      ;

    // note: '//' are added to the end to make clang-format work properly
}

Config::~Config() {}

const std::string&
Config::getListenAddress() const
{
    return listen_address_;
}

const std::string&
Config::getSrvAddress() const
{
    return srv_address_;
}

const std::string&
Config::getSrvPassword() const
{
    return srv_password_;
}

const bool&
Config::getIfSrvStarts() const
{
    return if_srv_starts_;
}

const bool&
Config::getIfHelp() const
{
    return if_help_;
}

const bool&
Config::getIfVersion() const
{
    return if_version_;
}

log::SeverityLevel
Config::getSevThes() const
{
    return (log::SeverityLevel)(4 - verbosity_);
}

const Keybinds&
Config::getKeybinds() const
{
    return keybinds_;
}

std::string
Config::getHelpMessage() const
{
    std::stringstream ss;
    ss << "Usage (as client): " << lchess_command_
       << " -s [ADDRESS:PORT] [OPTIONS]...\n"
       << "      (as server): " << lchess_command_
       << " -l [ADDRESS:PORT] [OPTIONS]...\n"
       << "Run LANChess, an online TUI chess game.\n\n";
    ss << *od_cli_;
    return ss.str();
}

}  // namespace lchess::config
