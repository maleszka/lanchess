// ========================================================================== //

// Copyright (C) 2024 Adam Maleszka

// This file is part of lanchess. lanchess is free software: you can
// redistribute it and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.

// lanchess is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// lanchess. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#include <algorithm>
#include <boost/algorithm/string.hpp>
#include <lchess/config/keybinds.hpp>
#include <lchess/term/key.hpp>
#include <sstream>

namespace lchess::config
{

bool
Keybind::operator==(const term::Key& key) const
{
    return (std::find(this->keys.begin(), this->keys.end(), key.nsText)
            != this->keys.end());
}

std::istream&
operator>>(std::istream& is, Keybind& kbd)
{
    // OPTION FORMAT: <some-stuff>,<some-more-stuff>,j,k

    std::string read_str;
    std::getline(is, read_str);
    boost::trim(read_str);

    boost::split(kbd.keys, read_str, boost::is_any_of(","));

    return is;
}

}  // namespace lchess::config
