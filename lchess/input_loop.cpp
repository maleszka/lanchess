// ========================================================================== //

// Copyright (C) 2024 Adam Maleszka

// This file is part of lanchess. lanchess is free software: you can
// redistribute it and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.

// lanchess is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// lanchess. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#include <csignal>
#include <lchess/input_loop.hpp>

namespace lchess
{

InputLoop::InputLoop(std::shared_ptr<EventQueue> queue)
  : queue_(queue)
{
    BOOST_LOG_FUNCTION();
    instances_.emplace_back(this);
    LOG(logger_, info) << "starting input thread";
    thread_.reset(new std::thread(&InputLoop::run, this));
}

InputLoop::~InputLoop()
{
    BOOST_LOG_FUNCTION();
    LOG(logger_, debug) << "waiting for input thread to stop";
    std::erase(instances_, this);
    if_running_ = false;
    thread_->join();
    LOG(logger_, info) << "stopped input thread";
}

void
InputLoop::run()
{
    // Register kernel signal handlers
    std::signal(SIGWINCH, InputLoop::sigStaticResizeHandler);
    std::signal(SIGTERM, InputLoop::sigStaticExitHandler);
    std::signal(SIGINT, InputLoop::sigStaticExitHandler);

    // Read keys
    if_running_ = true;
    term::Key key;
    int result;
    while (if_running_) {
        result = ki_.getKey(key);
        if (result == -1)
            queue_->emplaceEvent(
              EventType::inputFailure, std::any(), EventPriority::signal);
        else if (result == 1)
            queue_->emplaceEvent(
              EventType::keyPressed, key, EventPriority::input);
    }
}

void
InputLoop::sigResizeHandler(int signal)
{
    queue_->emplaceEvent(EventType::sigResize, signal, EventPriority::signal);
}

void
InputLoop::sigExitHandler(int signal)
{
    queue_->emplaceEvent(EventType::sigExit, signal, EventPriority::critical);
}

}  // namespace lchess
